def gradient(x, y, learning_rate=0.00001, max_error=0.2, gamma=0.5):
    """
    :param gamma:
    :param max_error:
    :param learning_rate:
    :param x: X - list of lists, with input data (16 values)
    :param y: Y - list of lists, output data (2 values)
    :return:
    """
    coefs, free_term = [0] * len(x[0]), 0

    while True:
        error = compute_error_gradient(x, y, coefs, free_term)
        if abs(error) < max_error:
            break

        new_free_term = free_term - learning_rate * error
        new_coefs = [coef - learning_rate * error * gamma for coef in coefs]

        coefs = new_coefs
        free_term = new_free_term

    return coefs, free_term


def compute_error_gradient(input, output, coefs, free_term):
    return sum(compute_error_forone(pacient, out, coefs, free_term) for pacient, out in zip(input, output))


def compute_error_forone(x, y, coefs, free_term):
    """
    :param x: one pacient input
    :param y: one pacient output
    :param coefs: computed coefs
    :param free_term: computed free_term
    :return:
    """
    return free_term + sum(elem * coef for elem, coef in zip(x, coefs)) - y[0]


def test_compute_error():
    x = [1, 1, 1, 1]
    y = [10]
    coefs, free_term = [0, 0, 0, 0], 0
    assert compute_error_forone(x, y, coefs, free_term) == -10

    coefs = [1, 2, 3, 4]
    assert compute_error_forone(x, y, coefs, free_term) == 0


def test_compute_total():
    x = [[1, 1, 1, 1], [2, 2, 2, 2]]
    y = [[10], [16]]

    coefs, free_term = [0, 0, 0, 0], 0
    assert compute_error_gradient(x, y, coefs, free_term) == -26

    coefs = [1, 2, 3, 4]
    free_term = -1
    assert compute_error_gradient(x, y, coefs, free_term) == 2


def test_gradient():
    # best: learning_rate=0.00001, max_error=0.2, gamma=0.5
    x = [[1, 1, 1, 1], [2, 2, 2, 2]]
    y = [[10], [16]]
    coefs, free_term = gradient(x, y)

    rez = free_term
    for elem, coef in zip([2, 2, 2, 2], coefs):
        rez += elem * coef

    assert rez - y[1][0] < 1

    x = [[2], [3], [5], [7], [9]]
    y = [[4], [5], [7], [10], [15]]
    coefs, free_term = gradient(x, y)

    print(3 * coefs[0] + free_term)


if __name__ == "__main__":
    test_compute_error()
    test_compute_total()
    test_gradient()