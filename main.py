import csv

from gradientdescent import gradient, compute_error_gradient
from utils import print_green

'''
print(stats.linregress(test_x, test_y))
print(least_squares_regression(test_x, test_y))
'''

# primele 4: subject#, age, sex, test_time
# 5, 6: motor, total UPDRS (y)
# restul: 16 voice measures  (22 de float-uri in total)
# trebuie sa aflu mai multi de a + un singur b
from error_square import error_single_param, error_multiple_params, compute_error
from leastsquare import multiple_params

ERROR = False


def read_from_csv(path):
    """

    :param path: path to the file
    :return: X, Y -> list of lists (o lista pentru fiecare pacient)
    """
    x, y = [], []
    with open(path) as csvfile:
        reader = csv.reader(csvfile, quoting=csv.QUOTE_NONNUMERIC)
        for row in reader:
            x.append(row[6:])
            y.append(row[4:6])

    return x, y


def error_parkinson_data_square(coef, free_term):
    """
    :param coef: coef for data
    :param free_term:  free term for data
    :return:
    """
    x, y = read_from_csv('data_from_user.txt')
    # y[0] -> doar primul din cei de iesire (motor_UPDRS)
    print_green("Err square: " + str(compute_error(x[0], y[0][0], coef, free_term)))


def error_parkinson_data_gradient(coefs, free_term):
    """
    :param coef: coefs for data
    :param free_term:  free term for data
    :return:
    """
    x, y = read_from_csv('data_from_user.txt')
    # y[0] -> doar primul din cei de iesire (motor_UPDRS)
    print_green("Err gradient: " + str(compute_error_gradient(x, y, coefs, free_term)))


def compute_motor_UDPRS_square(coef, free_term):
    # _, deoarece fisierul are acelasi format ca cel de intrare (nu folosesc y)
    x, _ = read_from_csv('data_from_user.txt')
    return free_term + sum(elem * coef for elem in x[0])


def compute_motor_UDPRS_gradient(coefs, free_term):
    x, _ = read_from_csv('data_from_user.txt')
    return free_term + sum(elem * coef for elem, coef in zip(x[0], coefs))


def train_square():
    x, y = read_from_csv('parkinsons_updrs.data.txt')
    coef, free_term = multiple_params(x, y)
    return coef, free_term


def train_gradient():
    x, y = read_from_csv('parkinsons_updrs.data.txt')
    coefs, free_term = gradient(x, y)
    return coefs, free_term


def main():
    coef, term = train_square()
    error_parkinson_data_square(coef, term)
    print("Motor_UDPRS: ", compute_motor_UDPRS_square(coef, term))

    coefs, term = train_gradient()
    error_parkinson_data_gradient(coefs, term)
    print("Motor_UDPRS (gradient): ", compute_motor_UDPRS_gradient(coefs, term))


if __name__ == "__main__":
    if ERROR:
        error_single_param()
        error_multiple_params()

    main()

