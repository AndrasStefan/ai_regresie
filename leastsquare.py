def least_squares_regression(x, y):
    """
    :param x: input data
    :param y: expected output
    :return: slope and free term
    """
    assert len(x) == len(y)

    sum_x, sum_y, sum_x2, sum_x_y, n = 0, 0, 0, 0, len(x)
    for varX, varY in zip(x, y):
        sum_x += varX
        sum_y += varY
        sum_x2 += varX * varX
        sum_x_y += varX * varY

    slope = (n * sum_x_y - sum_x * sum_y) / (n * sum_x2 - sum_x * sum_x)
    b = (sum_y - slope * sum_x) / n
    return slope, b


def multiple_params(x, y):
    assert len(x) == len(y)

    sum_x, sum_y, sum_x2, sum_x_y, n = 0, 0, 0, 0, len(x)

    for varX, varY in zip(x, y):
        # varY[0] -> doar primul din cei de iesire (motor_UPDRS)
        sum_varx, sum_vary = sum(varX), varY[0]
        sum_x += sum_varx
        sum_y += sum_vary
        sum_x2 += sum_varx ** 2
        sum_x_y += sum_vary * sum_varx

    slope = (n * sum_x_y - sum_x * sum_y) / (n * sum_x2 - sum_x * sum_x)
    b = (sum_y - slope * sum_x) / n
    return slope, b


def multiple_params_linear_regression_bad(x, y):
    rez, free_term, nr_of_params = [], 0, len(x[0])

    #only for y[0]

    col_rez = [row[0] for row in y]
    for i in range(nr_of_params):
        col = [row[i] for row in x]
        slope, b = least_squares_regression(col, col_rez)
        free_term += b
        rez.append(slope)

    return rez, free_term