from leastsquare import least_squares_regression, multiple_params
from utils import print_green


def compute_error(input, output, coef, free_term):
    """
    :param input: lista cu parametrii de intrare -> list
    :param output: rezultatul asteptat -> int
    :return: diferenta dintre rezultatul asteptap si cel obtinut
    """
    return free_term + sum(elem * coef for elem in input) - output


def error_single_param():
    print_green("Error single param")
    x = [2, 3, 5, 7, 9]
    y = [4, 5, 7, 10, 15]
    coef, free_term = least_squares_regression(x, y)

    for i, elem in enumerate(x):
        rez = elem * coef + free_term
        print("Err :" + str(rez - y[i]))


def error_multiple_params():
    print_green("Error multiple params")
    x = [[1, 3], [2, 4], [3, 5], [4, 7]]
    y = [[3], [8], [15], [28]]
    coef, free_term = multiple_params(x, y)

    for i, elem in enumerate(x):
        rez = elem[0] * coef + elem[1] * coef + free_term
        print("Err :" + str(rez - y[i][0]))
